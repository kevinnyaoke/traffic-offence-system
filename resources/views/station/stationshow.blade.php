@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>STATIONS</b></div>

                <div class="card-body">
                    {{$station->links()}}
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                    @include('includes.message')

                    <div class="container">

                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Base Commander</th>
                                    <th>Region</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="">
                                @foreach($station as $station)
                                <tr>

                                    <td>{{$station->name}}</td>
                                    <td>{{$station->base_commander}}</td>
                                    <td>{{$station->region}}</td>
                            
                                    <td><a class="btn btn-danger" href="{{route ('delstation',['id'=>$station->id])}}">Delete</a></td>
                                    <td><a class="btn btn-success" href="{{route ('editstation',['id'=>$station->id])}}">Edit</a></td>

                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
@endsection