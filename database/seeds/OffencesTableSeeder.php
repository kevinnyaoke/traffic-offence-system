<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class OffencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 1; $i <= 50; $i++) {
            App\Offence::create([
                'vehicle_registration' => 'XXXXXXXX',
                'type_of_offence' => 'Road offence',
                'description' => 'overspeeding',
                'penalty'=>'Offence penalty',
                'status'=>'offence status',
                'date_stated'=>$faker->date,
                'date_closed'=>$faker->date,
                'fine'=>'100,000',
                'officer_attached'=>$faker->name
            ]);
        }
    }
}
