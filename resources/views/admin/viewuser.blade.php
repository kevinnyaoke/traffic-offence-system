@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Registered Users</b></div>

                <div class="card-body">
                {{$users->links()}}
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    @include('includes.message')
                
                    <div class="container">

                                    <table class="table table-hover" id="myTable">
                                        <thead>
                                            <tr>
                                                <!-- <th>#</th> -->
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>File</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="">
                                            @foreach($users as $users)
                                            <tr>
                                                <!-- <td>{{$users->id}}</td> -->
                                                <td>{{$users->name}}</td>
                                                <td>{{$users->email}}</td>
                                                <td>{{$users->file}}</td>
                                                <td></td>
                                        
                                                <td><a class="btn btn-danger" href="{{route('delete',['id'=>$users->id])}}">Delete</a></td>
                                                <td><a class="btn btn-success" href=" {{route('edit',['id'=>$users->id])}}">Edit</a></td>
                     
                                            </tr>
                                            @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
