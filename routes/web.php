<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('login/custom',[
    'uses'=>'CustomController@login',
    'as'=>'customlogin',
]);

Route::group(['middlewere'=>'auth'], function(){
    Route::get('/dashboard','CustomController@dashboard')->name('dashboard');
    Route::get('/userhome', 'CustomController@userhome')->name('userhome');
    });
Route::get('admin/adduser','CustomController@adduser')->name('adduser');
Route::get('viewuser','CustomController@viewuser')->name('viewuser');
Route::post('admin/postuser','CustomController@postuser')->name('postuser');
Route::post('admin/update','CustomController@updateuser')->name('updateuser');
Route::post('admin/postfile','CustomController@postfile')->name('postfile');
Route::get('admin/newrule','CustomController@newrule')->name('newrule');
Route::post('admin/postrule','CustomController@postrule')->name('postrule');
Route::post('admin/updaterule','CustomController@updaterule')->name('updaterule');
Route::get('admin/viewrule','CustomController@viewrule')->name('viewrule');
Route::get('admin/addupdate','CustomController@addupdate')->name('addupdate');
Route::post('admin/postupdate','CustomController@postupdate')->name('postupdate');
Route::get('admin/viewupdate','CustomController@viewupdate')->name('viewupdate');
Route::post('admin/updateup','CustomController@updateup')->name('updateup');
Route::get('/securitycode','OtpController@getSecurityCode');


Route::get('user/viewrules','CustomController@rules')->name('user.rules');
Route::get('user/viewupdates','CustomController@updates')->name('user.updates');

Route::get('delete/{id}',[
    'uses'=>'CustomController@delete',
    'as'=>'delete'
]);

Route::get('deleterule/{id}',[
    'uses'=>'CustomController@deleterule',
    'as'=>'deleterule'
]);

Route::get('delupdate/{id}',[
    'uses'=>'CustomController@delupdate',
    'as'=>'delupdate'
]);

Route::get('editupdate/{id}',[
    'uses'=>'CustomController@editupdate',
    'as'=>'editupdate'
]);

Route::get('edit/{id}',[
 'uses'=>'CustomController@edit',
 'as'=>'edit'
]);

Route::get('editrule/{id}',[
    'uses'=>'CustomController@editrule',
    'as'=>'editrule'
   ]);

   Route::get('account/security_code', 'OtpController@getSecurityCode')->name('security_code');
    Route::post('account/security_code_verify', 'OtpController@verifySecurityCode')->name('security_code_verify');
    Route::get('account/security_code_verified/{token}', 'OtpController@verifiedSecurityCode')->name('security_code_verified');
    Route::post('account/security_code', 'OtpController@postSecurityCode')->name('post_security_code');

    Route::resource('cases','CaseController');
    Route::resource('stations','StationController');
    Route::resource('offences','OffenceController');
    Route::resource('offenders','OffenderController');

    Route::get('/creatoffence','OffenceController@create')->name('createoffence');
    Route::get('/showoffence','OffenceController@show')->name('showoffence');
    Route::post('/updateoffence','OffenceController@update')->name('updateoffence');

    Route::get('/createcase','CaseController@create')->name('createcase');
    Route::get('/showcase','CaseController@show')->name('showcase');
    Route::post('/updatecase','CaseController@update')->name('updatecase');

    Route::get('/createoffender','OffenderController@create')->name('createoffender');
    Route::get('/showoffender','OffenderController@show')->name('showoffender');
    Route::post('/updoffender','OffenderController@update')->name('updateoffender');

    Route::get('/createstation','StationController@create')->name('createstation');
    Route::get('/showstation','StationController@show')->name('showstation');
    Route::post('/updstation','StationController@update')->name('updatestation');

    Route::post('/addcase','CaseController@store')->name('storecase');
    Route::post('/addoffence','OffenceController@store')->name('storeoffence');
    Route::post('/addstation','StationController@store')->name('storestation');
    Route::post('/addoffender','OffenderController@store')->name('storeoffender');
  

    Route::get('deletecase/{id}',[
        'uses'=>'CaseController@destroy',
        'as'=>'deletecase'
    ]);
    Route::get('editcase/{id}',[
        'uses'=>'CaseController@edit',
        'as'=>'editcase'
       ]);

    Route::get('deleteoffence/{id}',[
        'uses'=>'OffenceController@destroy',
        'as'=>'deleteoffence'
    ]);

    Route::get('editoffence/{id}',[
        'uses'=>'OffenceController@edit',
        'as'=>'editoffence'
       ]);

    Route::get('deloffender/{id}',[
        'uses'=>'OffenderController@destroy',
        'as'=>'deloffender'
    ]);
    Route::get('editoffender/{id}',[
        'uses'=>'OffenderController@edit',
        'as'=>'editoffender'
       ]);

    Route::get('delstation/{id}',[
        'uses'=>'StationController@destroy',
        'as'=>'delstation'
    ]);
    Route::get('editstation/{id}',[
        'uses'=>'StationController@edit',
        'as'=>'editstation'
       ]);

 
       