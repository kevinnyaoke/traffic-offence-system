<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $fillable=[

        'name',
        'base_commander',
        'region',
        'offence_id',
    ];

    public function offence()
    {
        return $this->hasMany('App\Offence');
    }
}
