@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>OFFENCES</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Fill in the provided fields</h5>
                    @include('includes.message')


                    <form role="form" method="POST" action="{{route ('updateoffence')}}">
                   
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Vehicle Reg:</label>
                            <input type="text" class="form-control" id="vehicle_registration" name="vehicle_registration" required value="{{$edit->vehicle_registration}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Type Of offence:</label>
                            <input type="text" class="form-control" id="type_of_offence" name="type_of_offence"   required value="{{$edit->type_of_offence}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Description:</label>
                            <input type="text" class="form-control" id="description" name="description" required value="{{$edit->description}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Penalty:</label>
                            <input type="text" class="form-control" id="penalty" name="penalty" required value="{{$edit->penalty}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Status:</label>
                            <input type="text" class="form-control" id="status" name="status" required value="{{$edit->status}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Date Stated:</label>
                            <input type="date" class="form-control" id="date_stated" name="date_stated" required value="{{$edit->date_stated}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Date Closed:</label>
                            <input type="date" class="form-control" id="date_closed" name="date_closed" required value="{{$edit->date_closed}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Fine:</label>
                            <input type="text" class="form-control" id="fine" name="fine" required value="{{$edit->fine}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Officer Attached:</label>
                            <input type="text" class="form-control" id="officer_attached" name="officer_attached" required value="{{$edit->officer_attached}}">
                        </div>
                        <input type="text" id="user_id" name="user_id" hidden>
                        <input type="text" id="id" name="id" value="{{$edit->id}}" hidden>
 
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection