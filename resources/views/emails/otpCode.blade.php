@component('mail::message')
# Dear {{ Auth::user()->first_name }},

Here is your secret code  to user to access Traffic system.

<h2 style="color: #254CFF">{{ $otp->otp_code }} </h2>

Thanks,<br>
{{ config('app.name') }}
@endcomponent