<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    protected $fillable=[
        'offence_name',
        'file_name',
        'appeal',
        'status',
        'offence_id',
    ];

    public function offence()
    {
        return $this->hasMany('App\Offence');
    }
}
