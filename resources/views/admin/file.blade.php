@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Add user file</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form role="form" method="POST" action="{{route ('postfile')}}">

                        @csrf
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" 
                                value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">File:</label>
                            <input type="file" class="form-control" id="file" name="file" required>

                        </div>
                        <input type="text" id="id" name="id" value="{{$user->id}}" hidden>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection