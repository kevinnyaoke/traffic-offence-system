<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class StationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 1; $i <= 50; $i++) {
            App\Station::create([
                'name' => 'Police Station',
                'base_commander' => $faker->name,
                'region' => 'police station region',
            ]);
        }
    }
}
