<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Trafficsystem') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('bootstrap/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap/css/.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap/fonts/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->

                        <div>
                            <ul class="nav nav-pills nav-justified">
                                <li><a class="btn btn-info" href="{{route ('dashboard')}}">Home</a></li>
                                &nbsp
                                <li><a class="btn btn-info" href="{{route ('adduser')}}">Add Users</a></li>
                                &nbsp
                                <li><a class="btn btn-info" href="{{route ('viewuser')}}">View Users</a></li>
                                &nbsp
                               
                                <div class="dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button"
                                        data-toggle="dropdown">Traffic Rules
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a class="btn btn-info" href="{{route ('newrule')}}">Add new</a></li>
                                        <li><a class="btn btn-info" href="{{route ('viewrule')}}">View existing rules</a></li>
                                    </ul>
                                </div>&nbsp
                                <div class="dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button"
                                        data-toggle="dropdown">Other Updates
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a class="btn btn-info" href="{{route ('addupdate')}}">Add new</a></li>
                                        <li><a class="btn btn-info" href="{{route ('viewupdate')}}">View recent Updates</a></li>
                                    </ul>
                                </div>&nbsp
                                <div class="dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button"
                                        data-toggle="dropdown">Offences
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a class="btn btn-info" href="{{route ('createoffence')}}">Add new offence</a></li>
                                        <li><a class="btn btn-info" href="{{route ('showoffence')}}">View Offences</a></li>
                                    </ul>
                                </div>&nbsp
                                <div class="dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button"
                                        data-toggle="dropdown">Cases
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a class="btn btn-info" href="{{route ('createcase')}}">Add new case</a></li>
                                        <li><a class="btn btn-info" href="{{route ('showcase')}}">View Cases</a></li>
                                    </ul>
                                </div>&nbsp
                                <div class="dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button"
                                        data-toggle="dropdown">Station
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a class="btn btn-info" href="{{route ('createstation')}}">Add Sation</a></li>
                                        <li><a class="btn btn-info" href="{{route ('showstation')}}">View Station</a></li>
                                    </ul>
                                </div>
                                <div class="dropdown">
                                    <button class="btn btn-info dropdown-toggle" type="button"
                                        data-toggle="dropdown">Offender
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a class="btn btn-info" href="{{route ('createoffender')}}">Add new Offender</a></li>
                                        <li><a class="btn btn-info" href="{{route ('showoffender')}}">View Offenders</a></li>
                                    </ul>
                                </div>&nbsp

                            </ul>

                        </div>&nbsp
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

</html>