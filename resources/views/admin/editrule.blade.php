@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Make changes on the rule</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form role="form" method="POST" action="{{route ('updaterule')}}">

                        @csrf

                        <div class="form-group">
                            <label for="name">Name/Type:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$rules->name}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea class="form-control" rows="5" id="description"
                                name="description">{{$rules->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="section">Section:</label>
                            <input type="text" class="form-control" id="section" name="section"
                                value="{{$rules->section}}">
                        </div>
                        <div class="form-group">
                            <label for="sprice">Date:</label>
                            <input type="date" class="form-control" id="date" name="date" value="{{$rules->date}}">
                        </div>

                        <div class="form-group">
                            <label for="date">Effect from:</label>
                            <input type="date" class="form-control" id="effect_from" name="effect_from"
                                value="{{$rules->effect_from}}">
                        </div>
<input type="text" id="id" name="id" value="{{$rules->id}}" hidden>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection