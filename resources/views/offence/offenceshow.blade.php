@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>OFFENCES</b></div>

                <div class="card-body">
                    {{$offence->links()}}
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                    @include('includes.message')

                    <div class="container">

                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>

                                    <th>Vehicle Reg</th>
                                    <th>Type of Offence</th>

                                    <th>Penalty</th>
                                    <th>Status</th>
                                    <th>Date Stated</th>
                                    <th>date Closed</th>
                                    <th>Fine</th>
                                    <th>Officer Attached</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="">
                                @foreach($offence as $offence)
                                <tr>

                                    <td>{{$offence->vehicle_registration}}</td>
                                    <td>{{$offence->type_of_offence}}</td>

                                    <td>{{$offence->penalty}}</td>
                                    <td>{{$offence->status}}</td>
                                    <td>>{{$offence->date_stated}}</td>
                                    <td>>{{$offence->date_closed}}</td>
                                    <td>>{{$offence->fine}}</td>
                                    <td>>{{$offence->officer_attached}}</td>

                                    <td><a class="btn btn-danger"
                                            href="{{route ('deleteoffence',['id'=>$offence->id])}}">Delete</a></td>
                                    <td><a class="btn btn-success"
                                            href="{{route ('editoffence',['id'=>$offence->id])}}">Edit</a></td>

                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
@endsection