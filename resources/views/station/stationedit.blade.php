@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>STATIONS</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Fill in the provided fields</h5>
                    @include('includes.message')


                    <form role="form" method="POST" action="{{Route ('updatestation')}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" required value="{{$edit->name}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Base Commander:</label>
                            <input type="text" class="form-control" id="base_commander" name="base_commander" required value="{{$edit->base_commander}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Region:</label>
                            <input type="text" class="form-control" id="region" name="region" required value="{{$edit->region}}">
                        </div>
                        <input type="text" id="offence_id" name="offence_id" hidden>
                        <input type="text" id="id" name="id" value="{{$edit->id}}" hidden>
                    

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection