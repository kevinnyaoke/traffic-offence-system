@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Make changes on Updates</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('includes.message')

                    <form role="form" method="POST" action="{{route ('updateup')}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Date:</label>
                            <input type="date" class="form-control" id="date" name="date" required value="{{$update->date}}">
                        </div>
                        <div class="form-group">
                            <label for="content">Content:</label>
                            <textarea class="form-control" rows="5" id="content" name="content"
                                placeholder="Write update">{{$update->content}}</textarea>
                        </div>
                        <input type="text" id="id" name="id" value="{{$update->id}}" hidden>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
