<?php

namespace App\Http\Controllers;

use App\Station;
use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('station.stationcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = Station::create($request->all());
        return redirect()->back()->with('success', 'successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function show(Station $station)
    {
        $station = Station::orderBy('id', 'desc')->paginate('15');
        return view('station.stationshow', compact('station'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Station::find($id);
        return view('station.stationedit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'base_commander'=>'required',
            'region'=>'required',
    
        ]);

        $id=$request->input('id');
        $name=$request->input('name');
        $base_commander=$request->input('base_commander');
        $region=$request->input('region');

        $station=Station::find($id);
        if(!$station){
            return redirect()->back()->with('error','Selected Station cannot be updated!');
        }
         $station->id=$id;
         $station->name=$name;
         $station->base_commander=$base_commander;
         $station->region=$region;
         $station->update();

         return redirect()->route('showstation')->with('success','Update was successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Station::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', 'Deleted!');

    }
}
