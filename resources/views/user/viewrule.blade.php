@extends('layouts.user')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Enter new user information</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Fill in the provided fields</h5>
                    @include('includes.message')
                    <div class="container">
                        {{$rule->links()}}
                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>section</th>
                                    <th>Date uploaded</th>
                                    <th>Effective From</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="">
                                @foreach($rule as $rule)
                                <tr>

                                    <td>{{$rule->name}}</td>
                                    <td>{{$rule->description}}</td>
                                    <td>{{$rule->section}}</td>
                                    <td>{{$rule->date}}</td>
                                    <td>{{$rule->effect_from}}</td>
                                    <td></td>

                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection