<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Otp;
use Auth;
use Mail;
use App\UsedOtp;
use Carbon\Carbon;
use App\Mail\OtpCreated;
use Illuminate\Http\Request;

class OtpController extends Controller
{
    
    public function postSecurityCode(Request $request)
    {
        $random = substr(md5(mt_rand()), 0, 7);
        $otp_random = strtoupper($random);

        $otp = new Otp;
        $otp->user_id = $request->user_id;
        $otp->otp_code = $otp_random;
        $otp->save();

        $user = Auth::user();
        Mail::to($user->email)
            ->cc('sonfordonyango@gmail.com')
            ->send(new OtpCreated($otp));
        return view('payment.payments.securityCode')->with('flash_message', 'The code has been sent to your email!');
    }

    public function getSecurityCode()
    {
        return view('payment.payments.securityCode');
    }

    public function verifySecurityCode(Request $request)
    {
        $this->validate($request, [
            'security_code' => 'required',
        ]);

        $user = Auth::user();
        $keyword = $request->get('security_code');
        $otp = Otp::where(['otp_code' => $keyword, 'user_id' => $user->id])->first();

        if ($otp) {
            if (Carbon::now()->gt(Carbon::parse($otp->created_at)->addMinutes(30))) {
                return redirect()->route('security_code')->withErrors('Your code has Expired get another');
            }
            $tkn = $keyword . $user->id . time();
            return redirect()->route('userhome', ['token' => encrypt($tkn)]);
        } else {
            return redirect()->route('security_code')->withErrors('The code is invalid');
        }
    }
}
