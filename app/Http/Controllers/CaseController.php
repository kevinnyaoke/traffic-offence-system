<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cases;
class CaseController extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('case.casecreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
        ]);

        $store=Cases::create($request->all());
        return redirect()->back()->with('success','Added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $show=Cases::orderBy('id','desc')->paginate('15');
        return view ('case.caseshow', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Cases::find($id);
        return view('case.caseedit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([

            'file_name'=>'required',
            'appeal'=>'required',
            'status'=>'required',
        ]);

        $id=$request->input('id');
        $filename=$request->input('file_name');
        $appeal=$request->input('appeal');
        $status=$request->input('status');

        $cases=Cases::find($id);
        if(!$cases){
            return redirect()->back()->with('error','Selected case cannot be updated!');
        }
         $cases->id=$id;
         $cases->file_name=$filename;
         $cases->appeal=$appeal;
         $cases->status=$status;
         $cases->update();

         return redirect()->route('showcase')->with('success','Update was successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $case=Cases::findOrFail($id);
        $case->delete();

        return redirect()->back()->with('success','Removed successfully');
    }
}
