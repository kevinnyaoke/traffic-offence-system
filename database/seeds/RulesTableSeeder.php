<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class RulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();
  
        for($i=1;$i<=50;$i++){
            App\Rules::create([
                'name'=>'Traffic Rule',
                'Description'=>'All School Buses to be painted Yellow.',
                'section'=>'Section 34A of The Traffict Act',
                'date'=>$faker->date,
                'effect_from'=>$faker->date,
    
            ]);
        }

    }
}
