@extends('layouts.user')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2><b>Welcome {{Auth::user()->name}}</b></h2></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  <h4>You are welcomed to our services! We help you have a safe drive and journey by ensuring road safety
                   are well maintained Traffic laws are the laws which govern traffic and regulate vehicles, while rules of the road are both the laws and the informal 
                  rules that may have developed over time to facilitate the orderly and timely flow of traffic.</h4>
                  <img height="300px" width="700px" src="{{asset ('/images/img.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
