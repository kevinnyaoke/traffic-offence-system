@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Updates</div>

                <div class="card-body">

                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @include('includes.message')
                    
                    {{$update->links()}}
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>

                                <th>Date uploaded</th>
                                <th>Traffic Updates</eth>

                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="">
                            @foreach($update as $update)
                            <tr>

                                <td>{{$update->date}}</td>
                                <td>{{$update->content}}</td>

                                <td></td>

                                <td><a class="btn btn-danger"
                                        href="{{route('delupdate',['id'=>$update->id])}}">Delete</a></td>
                                <td><a class="btn btn-success" href=" {{route('editupdate',['id'=>$update->id])}}">Edit</a>
                                </td>

                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection