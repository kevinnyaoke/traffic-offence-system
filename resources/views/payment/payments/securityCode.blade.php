@extends('layouts.app')

@section('content')
<div class="container"> 
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b></b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Enter Security code</h5>
                    @include('includes.message')


                    <form action="{{ route('post_security_code') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                    <input type="hidden" name="otp_code">
                                <button type="submit" class="btn btn-primary btn-sm">Click to Get the Security Code</button>
                            </form>
                             <br/>
                            <form action="{{ route('security_code_verify') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="security_code">Enter security code from email:</label>
                                    <input name="security_code" type="text" class="form-control" id="security_code" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm"> Verify The Code</button>
                            </form>
                        <br/>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection