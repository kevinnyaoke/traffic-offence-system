<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $table = 'otps';

    protected $primaryKey = 'id';


    protected $fillable = ['user_id', 'otp_code'];

    public function otp()
    {
        return $this->belongsTo('App\User');
    }
}
