@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>OFFENDER</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Fill in the provided fields</h5>
                    @include('includes.message')


                    <form role="form" method="POST" action="{{route ('updateoffender')}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">First Name:</label>
                            <input type="text" class="form-control" id="fname" name="fname" required value="{{$edit->fname}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Last Name:</label>
                            <input type="text" class="form-control" id="lname" name="lname"   required value="{{$edit->lname}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Vehicle reg:</label>
                            <input type="text" class="form-control" id="description" name="vehicle_registration" required value="{{$edit->vehicle_registration}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Driver Licence:</label>
                            <input type="text" class="form-control" id="driver_licence" name="driver_licence" required value="{{$edit->driver_licence}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Gender:</label>
                            <input type="text" class="form-control" id="gender" name="gender" required value="{{$edit->gender}}">
                        </div>
                        <input type="text" id="offence_id" name="offence_id" hidden>
                    <input type="text" id="id" name="id"  value="{{$edit->id}}" hidden>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection