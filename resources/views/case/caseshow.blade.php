@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>CASES</b></div>

                <div class="card-body">
                    {{$show->links()}}
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                    @include('includes.message')

                    <div class="container">

                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>

                                    <th>File name</th>
                                    <th>Appeal</th>
                                    <th>Status</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="">
                                @foreach($show as $show)
                                <tr>

                                    <td>{{$show->file_name}}</td>
                                    <td>{{$show->appeal}}</td>
                                    <td>{{$show->status}}</td>


                                    <td><a class="btn btn-danger" href="{{route ('deletecase',['id'=>$show->id])}}">Delete</a></td>
                                    
                                    <td><a class="btn btn-success" href="{{route ('editcase',['id'=>$show->id])}}">Edit</a></td>

                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection