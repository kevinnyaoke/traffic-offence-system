<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UpdatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();

        for($i=1;$i<=50;$i++){
            App\Updates::create([
                'date'=>$faker->date,
                'content'=>'All PSV drivers should renew their driving licenses after every 2years.'
              ]);
        }
        
    }
}
