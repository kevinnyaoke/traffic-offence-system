<?php

namespace App\Http\Controllers;

use App\Offence;
use Illuminate\Http\Request;

class OffenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('offence.offencecreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'vehicle_registration'=>'required',
            'type_of_offence'=>'required',
            'description'=>'required',
            'penalty'=>'required',
            'status'=>'required',
            'date_stated'=>'required',
            'date_closed'=>'required',
            'fine'=>'required',
            'officer_attached'=>'required',

        ]);
        $store=Offence::create($request->all());
        return redirect()->back()->with('success','Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offence  $offence
     * @return \Illuminate\Http\Response
     */
    public function show(Offence $offence)
    {
        $offence=Offence::orderBy('id','desc')->paginate('15');
        return view('offence.offenceshow', compact('offence'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offence  $offence
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Offence::find($id);
        return view ('Offence.offenceedit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offence  $offence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([

            'vehicle_registration'=>'required',
            'type_of_offence'=>'required',
            'description'=>'required',
            'penalty'=>'required',
            'status'=>'required',
            'date_stated'=>'required',
            'date_closed'=>'required',
            'fine'=>'required',
            'officer_attached'=>'required',
        ]);

        $id=$request->input('id');
        $vehicle_registration=$request->input('vehicle_registration');
        $type_of_offence=$request->input('type_of_offence');
        $description=$request->input('description');
        $penalty=$request->input('penalty');
        $status=$request->input('status');
        $date_stated=$request->input('date_stated');
        $date_closed=$request->input('date_closed');
        $fine=$request->input('fine');
        $officer_attached=$request->input('officer_attached');

        $offence=Offence::find($id);
        if(!$offence){
            return redirect()->back()->with('error','Selected offence cannot be updated!');
        }
         $offence->id=$id;
         $offence->vehicle_registration=$vehicle_registration;
         $offence->type_of_offence=$type_of_offence;
         $offence->description=$description;
         $offence->penalty=$penalty;
         $offence->status=$status;
         $offence->date_stated=$date_stated;
         $offence->date_closed=$date_closed;
         $offence->fine=$fine;
         $offence->officer_attached=$officer_attached;
         $offence->update();

         return redirect()->route('showoffence')->with('success','Update was successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offence  $offence
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy=Offence::find($id);
        $destroy->delete();
        return redirect()->back()->with('success','deleted!');
    }
}
