<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RulesTableSeeder::class);
        $this->call(UpdatesTableSeeder::class);
        $this->call(CasesTableSeeder::class);
        $this->call(OffencesTableSeeder::class);
        $this->call(OffendersTableSeeder::class);
        $this->call(StationsTableSeeder::class);
    }
}
