<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offender extends Model
{
    protected $fillable=[

        'fname',
        'lname',
        'offence_id',
        'vehicle_registration',
        'driver_licence',
        'gender',
    ];

    public function offendce()
    {
        return $this->hasMany('App\Offence');
    }
}
