<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 1; $i <= 50; $i++) {
            App\Cases::create([
                'file_name' => 'file',
                'appeal' => 'Appeal',
                'status' => 'Status',
            ]);
        }
    }
}
