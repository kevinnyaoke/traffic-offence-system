<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offences', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('vehicle_registration');
            $table->string('type_of_offence');
            $table->string('description');
            $table->string('penalty');
            $table->unsignedInteger('user_id')->references('id')->on('users')->onDelete('cascade')->nullable();
            $table->string('status');
            $table->string('date_stated');
            $table->string('date_closed');
            $table->string('fine');
            $table->string('officer_attached');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offences');
    }
}
