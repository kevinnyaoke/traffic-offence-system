<?php

namespace App\Http\Controllers;

use App\Offender;
use Illuminate\Http\Request;

class OffenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('offender.offendercreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store=Offender::create($request->all());
        return redirect()->back()->with('success','Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function show(Offender $offender)
    {
        $show=Offender::orderBy('id','desc')->paginate('15');
        return view ('offender.offendershow', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Offender::find($id);
        return view('offender.offenderedit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'fname'=>'required',
            'lname'=>'required',
            'vehicle_registration'=>'required',
            'driver_licence'=>'required',
            'gender'=>'required'
        ]);

        $id=$request->input('id');
        $fname=$request->input('fname');
        $lname=$request->input('lname');
        $offence_id=$request->input('offence_id');
        $vehicle_registration=$request->input('vehicle_registration');
        $driver_licence=$request->input('driver_licence');
        $gender=$request->input('gender');

        $offender=offender::find($id);
        if(!$offender){
            return redirect()->back()->with('error','Selected offender cannot be updated!');
        }
         $offender->id=$id;
         $offender->fname=$fname;
         $offender->lname=$lname;
         $offender->offence_id=$offence_id;
         $offender->vehicle_registration=$vehicle_registration;
         $offender->driver_licence=$driver_licence;
         $offender->gender=$gender;
         $offender->update();

         return redirect()->route('showoffender')->with('success','Update was successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offender  $offender
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $destroy=Offender::find($id);
        $destroy->delete();
        return redirect()->back()->with('success','Deleted!');
    }
}
