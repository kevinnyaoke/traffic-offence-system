<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offence extends Model
{
    protected $fillable=[
        'vehicle_registration',
        'type_of_offence',
        'description',
        'penalty',
        'user_id',
        'status',
        'date_stated',
        'date_closed',
        'fine',
        'officer_attached'
    ];

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function offender()
    {
        return $this->hasMany('App\Offender');
    }

    public function cases()
    {
        return $this->hasMany('App\Cases');
    }

    public function station()
    {
        return $this->hasMany('App\Station');
    }
}
