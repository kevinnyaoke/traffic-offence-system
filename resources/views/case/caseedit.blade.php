@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>CASES</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Fill in the provided fields</h5>
                    @include('includes.message')


                    <form role="form" method="POST" action="{{route ('updatecase')}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="quantity">File Name:</label>
                            <input type="text" class="form-control" id="file_name" name="file_name" required value="{{$edit->file_name}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Appeal:</label>
                            <input type="text" class="form-control" id="appeal" name="appeal" required value="{{$edit->appeal}}">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Status:</label>
                            <input type="text" class="form-control" id="status" name="status" required value="{{$edit->status}}">
                        </div>

                        <input type="text" id="offence_id" name="offence_id" hidden value="{{$edit->offence_id}}">
                        <input type="text" id="id" name="id" value="{{$edit->id}}" hidden>


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection