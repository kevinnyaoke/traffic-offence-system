@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>OFFENDER</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <h5>Fill in the provided fields</h5>
                    @include('includes.message')


                    <form role="form" method="POST" action="{{route ('storeoffender')}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">First Name:</label>
                            <input type="text" class="form-control" id="fname" name="fname" required>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Last Name:</label>
                            <input type="text" class="form-control" id="lname" name="lname"   required>
                        </div>
                        <div class="form-group">
                            <label for="name">Vehicle reg:</label>
                            <input type="text" class="form-control" id="description" name="vehicle_registration" required> 
                        </div>
                        <div class="form-group">
                            <label for="quantity">Driver Licence:</label>
                            <input type="text" class="form-control" id="driver_licence" name="driver_licence" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Gender:</label>
                            <input type="text" class="form-control" id="gender" name="gender" required>
                        </div>
                        <input type="text" id="offence_id" name="offence_id" hidden>
                    

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection