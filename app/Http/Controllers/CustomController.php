<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\User;
use App\Rules;
use App\Updates;
use App\Offence;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomController extends Controller
{

    use UploadTrait;

    public function login(Request $request)
    {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ]));

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = User::where('email', $request->email)->first();
            if ($user->admin) {
                return redirect()->route('dashboard');
            }
            return redirect()->route('security_code');
        } else {
            return redirect()->back()->with('error', 'Your email or password is incorrect');
        }
    }

    public function userhome()
    {
        $user = Auth::user();
        return view('user.dashboard');
    }

    public function dashboard()
    {
        $offence=Offence::orderBy('id','desc')->paginate('15');
        return view('admin.dashboard', compact('offence'));
    }

    public function viewuser()
    {
        $users = User::orderBy('id','desc')->where('admin', '0')->paginate('15');
        return view('admin.viewuser', compact('users'));
    }

    public function adduser()
    {
        return view('admin.adduser');
    }

    public function postuser(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            
        ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $file = $request->input('file');

        User::create([
            'name' => $name,
            'email' => $email,
            'file'=> $file,
            'password' => Hash::make($email),
        ]);

        return redirect()->back()->with('success','user added successfully');

    }

    public function addfile($id)
    {
        $user = User::find($id);
        return view('admin.file', compact('user'));
    }

    public function postfile(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $user = User::findOrFail($id); // Get current user
        $user->id = $request->input('id'); // Set user name
        if ($request->has('file')) { // Check if a profile image has been uploaded
            $image = $request->file('file'); // Get image file
            $extension = $image->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $image->move('uploads/images/', $filename); // Define folder path
            $user->file = $filename; // Set user profile image path in database to filePath
        } else {
            return $request;
            $user->image = '';
        }

        $user->save(); // Persist user record to database

        $users = User::all()->where('admin', '0');
        return view('admin.viewuser', compact('users'))->with('success', 'update was successful!');
    }

    public function delete($id) {
        $remove = User::find($id);
        $remove->delete();
        return redirect()->back()->with('success', 'user deleted successfully');
    }

    public function edit($id)
    {
        $users = User::find($id);
        return view('admin.edituser', compact('users'));
    }

    public function updateuser(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $id = $request->input('id');
        $name = $request->input('name');
        $email = $request->input('email');

        $updateuser = User::find($id);
        if (!$updateuser) {
            return redirect()->back()->with('error', 'update not successful');
        }

        $updateuser->id = $id;
        $updateuser->name = $name;
        $updateuser->email = $email;
        $updateuser->update();

        $users = User::all()->where('admin', '0');
        return view('admin.viewuser', compact('users'))->with('success', 'update was successful!');

    }

    public function newrule(){
        return view('admin.newrule');
    }

    public function postrule(Request $request){
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'section'=>'required',
            'date'=>'required',
            'effect_from'=>'required'
        ]);

        $rule=Rules::create($request->all());
        return redirect()->back()->with('success','You have successfully added new traffic rule');
    }

    public function viewrule(){
        $rule=Rules::orderBy('id','desc')->paginate('15');
        return view('admin.viewrules', compact('rule'));
    }

    public function deleterule($id){
        $remove=Rules::find($id);
        $remove->delete();
        return redirect()->back()->with('success','Traffic rule is removed!');
    }

    public function editrule($id){
        $rules=Rules::find($id);
        return view('admin.editrule', compact('rules'));
    }

    public function updaterule(Request $request){
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'section'=>'required',
            'date'=>'required',
            'effect_from'=>'required'
        ]);

        $id=$request->input('id');
        $name=$request->input('name');
        $description=$request->input('description');
        $section=$request->input('section');
        $date=$request->input('date');
        $effect_from=$request->input('effect_from');

        $rules=Rules::find($id);
        if(!$rules){
            return redirect()->back()->with('error','Selected rule cannot be updated!');
        }
         $rules->id=$id;
         $rules->name=$name;
         $rules->description=$description;
         $rules->section=$section;
         $rules->date=$date;
         $rules->effect_from;
         $rules->update();

         $rule=Rules::orderBy('id','desc')->paginate('15');
         return redirect()->route('viewrule')->with('success','Update was successfull');
    }

    public function addupdate(){
        return view('admin.addupdate');
    }
    public function postupdate(Request $request){
        $request->validate([
            'date'=>'required',
            'content'=>'required'
        ]);

        $update=Updates::create($request->all());
        return redirect()->back()->with('success','Update was usuccessful!');
    }

    public function viewupdate(){
        $update=Updates::orderBy('id','desc')->paginate('15');
        return view('admin.viewupdate', compact('update'));
    }

    public function delupdate($id){
        $remove=Updates::find($id);
        if(!$remove){
            return redirect()->back()->with('error','Update cannot delete try later!');
        }

        $remove->delete();

        $update=Updates::orderBy('id','desc')->paginate('15');
        return redirect()->back()->with('success','Update is deleted!');;

    }

    public function editupdate($id){
        $update=Updates::find($id);
        return view('admin.editupdate',compact('update'));
    }

    public function updateup(Request $request){
        $request->validate([
            'date'=>'required',
            'content'=>'required',
        ]);

        $id=$request->input('id');
        $date=$request->input('date');
        $content=$request->input('content');

        $updates=Updates::find($id);
        if(!$updates){
            return redirect()->back()->with('error','Selected update cannot be updated!');
        }
         $updates->id=$id;
         $updates->date=$date;
         $updates->content=$content;
         $updates->update();

         $update=Updates::orderBy('id','desc')->paginate('15');
         return redirect()->route('viewupdate')->with('success','successful update!');
    }

    public function rules(){
        $rule=Rules::orderBy('id','desc')->paginate('15');
        return view('user.viewrule',compact('rule'));
    }

    
    public function updates(){
        $update=Updates::orderBy('id','desc')->paginate('15');
        return view('user.viewupdate',compact('update'));
    }
}