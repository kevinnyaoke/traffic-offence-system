@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>OFFFENDERS</b></div>

                <div class="card-body">
                    {{$show->links()}}
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                    @include('includes.message')

                    <div class="container">

                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>

                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>vehicle reg</th>
                                    <th>Driver Licence</th>
                                    <th>Gender</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="">
                                @foreach($show as $show)
                                <tr>

                                    <td>{{$show->fname}}</td>
                                    <td>{{$show->lname}}</td>
                                    <td>{{$show->vehicle_registration}}</td>
                                    <td>{{$show->driver_licence}}</td>
                                    <td>{{$show->gender}}</td>

                                    <td><a class="btn btn-danger" href="{{route ('deloffender',['id'=>$show->id])}}">Delete</a></td>
                                    <td><a class="btn btn-success" href="{{route ('editoffender',['id'=>$show->id])}}">Edit</a></td>
                                    <td><a class="btn btn-success" type="file" href="{{route ('editoffender',['id'=>$show->id])}}">Add File</a></td>

                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection