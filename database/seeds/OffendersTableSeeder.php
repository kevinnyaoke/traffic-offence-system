<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class OffendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 1; $i <= 50; $i++) {
            App\Offender::create([
                'fname' =>$faker->firstname,
                'lname' => $faker->lastname,
                'vehicle_registration' => 'XXXXXXXXXX',
                'driver_licence'=>'Licence Number',
                'gender'=>'Any Gender',
            ]);
        }
    }
}
